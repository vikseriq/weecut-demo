//
//  UIProcessIndicator.swift
//  weeCut
//
//  Created by vikseriq on 04.02.16.
//  Copyright © 2016 NewSite. All rights reserved.
//

import UIKit

/// Activity indicator for background running tasks
class UIProcessIndicator: UIView {

    var activityIndicator: UIActivityIndicatorView!
    let size: CGFloat = 80
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    init(view: UIView, fade: Bool){
        super.init(frame: CGRectMake(0, 0, view.bounds.width, view.bounds.height))
        self.hidden = true
        if fade {
            self.backgroundColor = UIColor(rgba: "#FFFFFF44")
            self.userInteractionEnabled = true
        } else {
            self.userInteractionEnabled = false
        }
        view.addSubview(self)
        setupView()
    }
    
    func setupView(){
        let backgroundView = UIView(frame: CGRectMake((self.bounds.width - size) / 2, (self.bounds.height - size) / 2, size, size))
        backgroundView.backgroundColor = UIColor(rgba: "#444444DD")
        backgroundView.clipsToBounds = true
        backgroundView.layer.cornerRadius = 10
        backgroundView.userInteractionEnabled = true
        
        let indicatorOffset: CGFloat = size / 2 - size / 4
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(indicatorOffset, indicatorOffset, size / 2, size / 2))
        activityIndicator.activityIndicatorViewStyle = .WhiteLarge
        
        backgroundView.addSubview(activityIndicator)
        self.addSubview(backgroundView)
    }
    
    func show(){
        self.hidden = false
        activityIndicator.startAnimating()
    }
    
    func dismiss(){
        activityIndicator.stopAnimating()
        self.hidden = true
    }

}
