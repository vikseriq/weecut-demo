//
//  UIBadgedButtonView.swift
//  weeCut
//
//  Created by vikseriq on 01.02.16.
//  Copyright © 2016 NewSite. All rights reserved.
//

import UIKit

class UIBadgedButtonView: UIButton {

    @IBInspectable var imageActive: UIImage! {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var imageInactive: UIImage! {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var badgeValue: Int = 0 {
        didSet {
            setupView()
        }
    }
    
    var badgeHighlighted: Bool = false {
        didSet {
            setupView()
        }
    }
    
    var imgView: UIImageView!
    var badgeLabel: UILabel!
    
    func initView(){
        let w = frame.width
        imgView = UIImageView(frame: CGRectMake(w * 0.55, 0, w * 0.45, frame.height))
        imgView.contentMode = .ScaleAspectFit
        imgView.clipsToBounds = true
        
        badgeLabel = UILabel(frame: CGRectMake(0, 0, w * 0.55, frame.height))
        badgeLabel.font = UIFont.systemFontOfSize(14)
        badgeLabel.textAlignment = .Center
        badgeLabel.textColor = UIColor.whiteColor()
        
        addSubview(imgView)
        addSubview(badgeLabel)
    }
    
    func setupView(){
        imgView.image = badgeHighlighted ? imageActive : imageInactive
        badgeLabel.text = badgeValue < 1000 ? "\(badgeValue)" : "\(round(Double(badgeValue) / 1000.0))k"
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
        setupView()
    }
    
}
