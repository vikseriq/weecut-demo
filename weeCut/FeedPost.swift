//
//  FeedPost.swift
//  weeCut
//
//  Created by vikseriq on 03.02.16.
//  Copyright © 2016 NewSite. All rights reserved.
//

import UIKit

class FeedPost: NSObject {

    var externalId = ""
    var image: UIImage?
    var imageUrl: NSURL?
    var text = ""
    var likesCount = 0
    var commentsCount = 0
    var isLiked = false
    var isCommented = false
    var authorId = ""
    var authorAvatar: UIImage?
    var authorAvatarUrl: NSURL?
    var authorName = ""
    var hashtags = ""
    var created = ""
    
    var _raw: NSDictionary!
    
    /// Init FeedPost from JSON-parsed object
    init(data: NSDictionary){
        _raw = data
        externalId = data["post_id"] as! String
        imageUrl = NSURL(string: data["image"] as! String)
        text = data["text"] as! String
        created = data["created"] as! String
        
        isCommented = (data["is_commented"] as! NSString).boolValue
        isLiked = (data["is_liked"] as! NSString).boolValue
        commentsCount = data["comments_count"] as! Int
        likesCount = data["likes_count"] as! Int

        authorAvatarUrl = NSURL(string: data["by_avatar"] as! String)
        authorName = data["display_name"] as! String
        
        // Yep, it's fast-n-dirty solution for quick app delivery - 'll rewrited
        image = UIImage(data: NSData(contentsOfURL: imageUrl!)!)
        authorAvatar = UIImage(data: NSData(contentsOfURL: authorAvatarUrl!)!)
    }
    
}
