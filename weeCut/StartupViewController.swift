//
//  StartupViewController.swift
//  weeCut
//
//  Created by vikseriq on 31.01.16.
//  Copyright © 2016 NewSite. All rights reserved.
//

import UIKit

class StartupViewController: UIViewController {
    
    @IBOutlet weak var labelTitle: UILabel!
    
    private let animationSpeed = 0.2
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        // Initial UI setup
        labelTitle.alpha = 0.0
        view.backgroundColor = UIColor.lightGrayColor()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        // Fade in title and perform login
        UIView.animateWithDuration(self.animationSpeed, animations: { () -> Void in
            self.labelTitle.alpha = 0.5
            }) { (_) -> Void in
                self.doLogin()
                UIView.animateWithDuration(self.animationSpeed, animations: { () -> Void in
                    self.labelTitle.alpha = 1
                    }
                )
        }
    }
    
    /// Perform login
    func doLogin(){
        WCNetwork.sharedInstance.login(nil, password: nil) { (data, error) -> Void in
            dispatch_async(dispatch_get_main_queue(), {
                if (error != nil){
                    // Show error message with dismiss and reload buttons
                    let alertController = UIAlertController(title: "Login failed", message: error!, preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Destructive, handler: nil))
                    alertController.addAction(UIAlertAction(title: "Reload", style: UIAlertActionStyle.Default, handler: { (_) -> Void in
                        // Trigger login process again
                        NSTimer.scheduledTimerWithTimeInterval(self.animationSpeed, target: self, selector: Selector("doLogin"), userInfo: nil, repeats: false)
                    }))
                    self.presentViewController(alertController, animated: true, completion: nil)
                } else {
                    self.loginSuccess()
                }
            })
        }
    }
    
    /// Login success – fade background and navigate to main UI
    func loginSuccess(){
        UIView.animateWithDuration(self.animationSpeed, animations: { () -> Void in
            self.view.backgroundColor = UIColor.whiteColor()
            }) { (_) -> Void in
                //print(WCNetwork.sharedInstance.currentUser)
                self.performSegueWithIdentifier("afterLogin", sender: self)
        }
    }
}

