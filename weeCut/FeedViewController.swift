//
//  FeedViewController.swift
//  weeCut
//
//  Created by vikseriq on 01.02.16.
//  Copyright © 2016 NewSite. All rights reserved.
//

import UIKit

private let reuseIdentifier = "feedCell"

class FeedViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!

    var processIndicator: UIProcessIndicator?
    var tabBar: UITabBar?
    var tabBarVisible: Bool = true
    var lastScrollOffset: CGFloat = 0
    
    var postsPage = 0
    var postsPerPage = 10
    var posts: [FeedPost] = []
    var pageLoading = false
    var totalPages = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        processIndicator = UIProcessIndicator(view: self.view, fade: false)
        
        tabBar = (parentViewController as! UITabBarController).tabBar
        let bottomGap = tabBar?.frame.height ?? 0
        
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: bottomGap, right: 0)
        bottomConstraint.constant = -bottomGap
        
        let spacer: CGFloat = 3
        let width = (view.frame.width - spacer) * 0.5
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .Vertical
        layout.itemSize = CGSizeMake(width, width * 1.5)
        layout.minimumInteritemSpacing = spacer
        layout.minimumLineSpacing = spacer
        collectionView.setCollectionViewLayout(layout, animated: false)
        
        collectionView.backgroundColor = UIColor.whiteColor()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.registerNib(UINib(nibName: "FeedCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        loadPosts()
    }
    
    func loadPosts(){
        if pageLoading || postsPage > totalPages {
            return
        }
        processIndicator?.show()
        pageLoading = true
        postsPage++
        WCNetwork.sharedInstance.getPosts(WCNetwork.sharedInstance.currentUser!["user_id"] as! String, page: postsPage, perPage: postsPerPage) { (data, error) -> Void in
            if error != nil {
                return self.loadFails()
            }
            guard let dataPosts = data!["posts"] as? [NSDictionary] else {
                return self.loadFails()
            }
            for post in dataPosts {
                self.posts.append(FeedPost(data: post))
            }
            
            self.loadSuccess(dataPosts.count)
        }
    }
    
    func loadSuccess(newPosts: Int){
        if newPosts > 0 {
            totalPages = postsPage
        }
        pageLoading = false
        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
            self.processIndicator?.dismiss()
            self.collectionView.reloadData()
        })
    }
    
    func loadFails(){
        pageLoading = false
        postsPage--
        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
            self.processIndicator?.dismiss()
        })
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! FeedCell
        cell.setupCell(posts[indexPath.row])
        return cell
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        var offset = scrollView.contentOffset.y
        if offset < 0 {
            offset = 0
        }
        var isScrollDown = offset > lastScrollOffset
        lastScrollOffset = offset
        
        let bottom = scrollView.contentSize.height - scrollView.frame.size.height + scrollView.contentInset.bottom
        if offset >= bottom {
            isScrollDown = true
            loadPosts()
        }
        
        if isScrollDown && tabBarVisible {
            tabBar?.toggle(false, animated: true)
            tabBarVisible = false
        } else if !isScrollDown && !tabBarVisible {
            tabBar?.toggle(true, animated: true)
            tabBarVisible = true
        }
    }

}
