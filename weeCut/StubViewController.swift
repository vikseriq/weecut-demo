//
//  StubViewController.swift
//  weeCut
//
//  Created by vikseriq on 01.02.16.
//  Copyright © 2016 NewSite. All rights reserved.
//

import UIKit

class StubViewController: UIViewController {

    var labelStub: UILabel!
    let labelHeight: CGFloat = 25.0
    
    override func viewDidLoad() {
        labelStub = UILabel()
        labelStub.text = "Quam Stub View"
        labelStub.textAlignment = .Center
        labelStub.textColor = UIColor.whiteColor()
        labelStub.font = UIFont.systemFontOfSize(labelHeight)
        
        view.addSubview(labelStub)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let width: CGFloat = view.frame.width * 0.75
        labelStub.frame = CGRectMake((view.frame.width - width) / 2, (view.frame.height - labelHeight) / 2, width, labelHeight)
    }
    
    override func viewWillAppear(animated: Bool) {
        labelStub.alpha = 0
        view.backgroundColor = UIColor.lightGrayColor()
    }
    
    override func viewDidAppear(animated: Bool) {
        UIView.animateWithDuration(0.5) { () -> Void in
            self.labelStub.alpha = 1
        }
    }
    
}
