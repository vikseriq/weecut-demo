//
//  FeedCell.swift
//  weeCut
//
//  Created by vikseriq on 01.02.16.
//  Copyright © 2016 NewSite. All rights reserved.
//

import UIKit

class FeedCell: UICollectionViewCell {

    @IBOutlet weak var imageBackground: UIImageView!
    @IBOutlet weak var imageProfile: UIRoundedImageView!
    @IBOutlet weak var buttonLike: UIBadgedButtonView!
    @IBOutlet weak var buttonComment: UIBadgedButtonView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func didMoveToSuperview() {
        backgroundColor = UIColor.lightGrayColor()
    }

    /// Setting up cell with FeedPost fields
    func setupCell(feedPost: FeedPost){
        buttonLike.badgeValue = feedPost.likesCount
        buttonLike.badgeHighlighted = feedPost.isLiked
        buttonComment.badgeValue = feedPost.commentsCount
        buttonComment.badgeHighlighted = feedPost.isCommented

        imageBackground.image = feedPost.image        
        imageProfile.image = feedPost.authorAvatar
    }
    
}
