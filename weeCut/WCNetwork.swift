//
//  WCNetwork.swift
//  weeCut
//
//  Created by vikseriq on 31.01.16.
//  Copyright © 2016 NewSite. All rights reserved.
//

import UIKit

/// Utilizing API communications with Parse REST endpoint
class WCNetwork: NSObject {

    typealias APIResponse = (NSDictionary?, String?) -> Void
    
    // singleton instance
    static let sharedInstance = WCNetwork()
    
    // current app configuration
    static let debug = true
    static let branch = "demo"
    private var config: NSDictionary
    
    // session token for API calls as authorized user
    private var session: String?
    
    internal var currentUser: NSDictionary?
    
    override init() {
        let dict = NSDictionary(contentsOfFile: NSBundle.mainBundle().pathForResource("Config", ofType: "plist")!)
        config = dict!["AppConfig_" + WCNetwork.branch] as! NSDictionary
    }
    
    /// Log into system with account data provided. 
    /// Session token for futher request will be obitained.
    func login(email: String?, password: String?, callback: APIResponse){
        let payload = [
            "email": email ?? config["user_email"] as! String,
            "password": password ?? config["user_password"] as! String,
            "app_name": config["app_name"] as! String,
            "app_version": NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as! String,
            "device_info": "Apple " + UIDevice.currentDevice().modelName
        ]
        requestAPI("/login", payload: payload) { (json, error) -> Void in
            if error != nil {
                return callback(nil, error)
            }
            guard let result = json else {
                return callback(json, "Bad response")
            }
            guard let token = result["sessionToken"] as? String else {
                return callback(result, "No token provided")
            }
            self.session = token
            self.currentUser = result["user"] as? NSDictionary
            callback(result, nil)
        }
    }
    
    /// Fetches posts for userId
    func getPosts(userId: String, page: Int, perPage: Int, callback: APIResponse){
        let payload = [
            "user_id": userId,
            "start_from": (page - 1) * perPage,
            "num_of_posts": perPage,
            "filter": "current_user_friends",
            "include_user_info": false,
            "include_is_in_dress": false
        ]
        requestAPI("/get_posts", payload: payload) { (json, error) -> Void in
                callback(json, error)
        }
    }
    
    /// Fetch user by userId
    func getUser(userId: String, callback: APIResponse){
                    let payload = [
                        "user_id": userId
                    ]
                    requestAPI("/get_user", payload: payload, callback: callback)
    }
    
    /// Perform request to REST endpoint.
    /// Passing payload will cause JSON request.
    /// Callback "data" will contain response_json["result"] on success
    /// otherwise "error" filled with error message
    func requestAPI(path: String, payload: NSDictionary?, callback: APIResponse){
        let url = path.hasPrefix("/") ? (config["api_endpoint"] as! String) + path : path
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        
        request.HTTPMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue(config["api_app_id"] as! String, forHTTPHeaderField: "X-Parse-Application-Id")
        request.addValue(config["api_app_key"] as! String, forHTTPHeaderField: "X-Parse-REST-API-Key")
        if let s = self.session {
            request.addValue(s, forHTTPHeaderField: "X-Parse-Session-Token")
        }
        
        // Non-nil payload means POST request with JSON data
        if payload != nil {
            do {
                let json = try NSJSONSerialization.dataWithJSONObject(payload!, options: [])
                request.HTTPMethod = "POST"
                request.HTTPBody = json
                if WCNetwork.debug {
                    // debug output of request
                    print("API Request", payload, " ", "\n")
                }
            } catch {}
        }
        
        // Performing request via system's networking session
        let session = NSURLSession.sharedSession()
        session.dataTaskWithRequest(request) { (data, resp, error) -> Void in
            if error != nil {
                return callback(nil, error!.localizedDescription)
            }
            guard let responseData = data else {
                return callback(nil, "No data received")
            }
            if WCNetwork.debug {
                // debug output or response
                let response = NSString(data: responseData, encoding: NSUTF8StringEncoding)
                print("API Response", response, " ", "\n")
            }
            
            // Assuming that response is JSON data with mandatory root fields "results" and "rc"
            let json: NSDictionary
            do {
                json = try NSJSONSerialization.JSONObjectWithData(responseData, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
            } catch {
                return callback(nil, "Can't parse response data")
            }
            guard let results = json["result"] as? NSDictionary else {
                return callback(json, json["rc"] as? String)
            }
            
            // Well done, pass clean results
            callback(results, nil)
        }.resume()
    }
    
}
