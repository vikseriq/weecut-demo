//
//  UIRoundedImageView.swift
//  weeCut
//
//  Created by vikseriq on 01.02.16.
//  Copyright © 2016 NewSite. All rights reserved.
//

import UIKit

class UIRoundedImageView: UIImageView {

    override func didMoveToSuperview() {
        layer.borderColor = UIColor.whiteColor().CGColor
        layer.borderWidth = 4
        layer.cornerRadius = self.frame.width / 2.0
        layer.masksToBounds = true
        clipsToBounds = true
    }
    
}
